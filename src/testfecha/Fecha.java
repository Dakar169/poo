/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testfecha;

/**
 *
 * @author Dakar_Vvsl
 */
public class Fecha {
    
    //Atributos en private
    private int dia;
    private int mes;
    private int anio;
    
    public String toString()
    {
    return dia+"/"+mes+"/"+anio;
    //Con el anterior metodo, ya es posible ejecutar en la clase TEST la instrucción
        //System.out.println(f); pues ya se sobreescribio el metodo toString
    }

    //El seiguiente es un constructor que tiene como argumentos de entrada 3 variables de tipo int que se 
    //asignan a los atributos del objeto
    public Fecha(int dia, int mes, int anio) {
        this.dia = dia;
        this.mes = mes;
        this.anio = anio;
    }

    //Este es el constructor en blanco o vacio o por defecto, el cual es necesario volver a 
    //enunciar al momento de crear uno nuevo
    public Fecha() {
    }
    
    public Fecha (String s)
    {
        //buscar las ocurrecncias de los separadores /
        int prime =s.indexOf(s);
        int segun =s.lastIndexOf(s);
        
        //Separar el día y asignarlo en el atributo de la clase.
        String sDia=s.substring(0,prime);
        dia=Integer.parseInt(sDia);
        
        //Separa el mes y asignarlo en el atributo de la clase.
        String sMes=s.substring(prime+1,segun);
        mes=Integer.parseInt(sMes);
        
        //Separa el año y asignalo en el atributo de la clase.
        String sAnio=s.substring(segun+1);
        anio=Integer.parseInt(sAnio);
        
        
        
        
    }
    
    
    public boolean equals(Object o)
    {
    Fecha otra = (Fecha)o;
    return (dia==otra.dia) && (mes==otra.mes) && (anio==otra.anio);
    }


//Es necesario crear los metodos de acceso para poder acceder a los atributos referenciados.

    /**
     * @return the dia
     */
    public int getDia() {
        return dia;
    }

    /**
     * @param dia the dia to set
     */
    public void setDia(int dia) {
        this.dia = dia;
    }

    /**
     * @return the mes
     */
    public int getMes() {
        return mes;
    }

    /**
     * @param mes the mes to set
     */
    public void setMes(int mes) {
        this.mes = mes;
    }

    /**
     * @return the anio
     */
    public int getAnio() {
        return anio;
    }

    /**
     * @param anio the anio to set
     */
    public void setAnio(int anio) {
        this.anio = anio;
    }
    
    
}